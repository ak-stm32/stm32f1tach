/*
 * Timer4.c
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */
#include "AK_TIM4.h"

TIM_HandleTypeDef _htim4;
//uint16_t _value = 0;

/**
 * Timer to emulate input, 1 per 1 msec (1/1000)
 */
void AK_TIM4_Init(void) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	__TIM4_CLK_ENABLE();
	_htim4.Instance = TIM4;
	_htim4.Init.Prescaler = AK_TIM4_Prescaler;
	_htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
	_htim4.Init.Period = AK_TIM4_Period;
	_htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	_htim4.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&_htim4);

	HAL_TIM_Base_Start_IT(&_htim4);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&_htim4, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&_htim4, &sMasterConfig);
}

void AK_TIM4_Msp_Init(void) {
	HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM4_IRQn);
}

TIM_HandleTypeDef* AK_TIM4_get_htim(void) {
	return &_htim4;
}

void TIM4_IRQHandler(void) {
	HAL_TIM_IRQHandler(&_htim4);
}


const struct _AK_TIM AK_TIM4 = {
    .Init = AK_TIM4_Init,
    .Msp_Init = AK_TIM4_Msp_Init,
	.htim = AK_TIM4_get_htim
};

