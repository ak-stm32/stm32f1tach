/*
 * Timer3.c
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */
#include "AK_TIM3.h"

TIM_HandleTypeDef _htim3;

/**
 * Timer for AK_TIME_COUNT, 1 per 1 msec (1/1000)
 */
void AK_TIM3_Init(void) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	__TIM3_CLK_ENABLE();
	_htim3.Instance = TIM3;
	_htim3.Init.Prescaler = AK_TIM3_Prescaler;
	_htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	_htim3.Init.Period = AK_TIM3_Period;
	_htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	_htim3.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&_htim3);

	HAL_TIM_Base_Start_IT(&_htim3);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&_htim3, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&_htim3, &sMasterConfig);
}

void AK_TIM3_Msp_Init(void) {
	HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM3_IRQn);
}

TIM_HandleTypeDef* AK_TIM3_Get_htim(void) {
	 return &_htim3;
}

void TIM3_IRQHandler(void) {
	HAL_TIM_IRQHandler(&_htim3);
}

const struct _AK_TIM AK_TIM3 = {
    .Init = AK_TIM3_Init,
    .Msp_Init = AK_TIM3_Msp_Init,
	.htim = AK_TIM3_Get_htim
};
