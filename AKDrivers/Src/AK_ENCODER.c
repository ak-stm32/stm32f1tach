/*
 * AK_ENCODER.c
 *
 *  Created on: 1 Aug 2016
 *      Author: akoiro
 */

#include "AK_ENCODER.h"

AK_ENCODER_Direction _AK_ENCODER_direction = None;
AK_ENCODER_Delegator _AK_ENCODER_delegator;

void _AK_ENCODER_Init_Pin(GPIO_TypeDef*, uint32_t, IRQn_Type);
void _AK_ENCODER_V00(AK_ENCODER_Def* def);
void _AK_ENCODER_V01(AK_ENCODER_Def* def);
void _AK_ENCODER_V10(AK_ENCODER_Def* def);
void _AK_ENCODER_V11(AK_ENCODER_Def* def);

void AK_ENCODER_Init(AK_ENCODER_Def* def) {
	__AK_GPIO_PORT_ENABLE(def->PORT);

	_AK_ENCODER_Init_Pin(def->PORT, def->CLK_PIN, def->CLK_IRQ);
	_AK_ENCODER_Init_Pin(def->PORT, def->DT_PIN, def->DT_IRQ);
	_AK_ENCODER_Init_Pin(def->PORT, def->SW_PIN, def->SW_IRQ);

	def->direction = None;
}

void AK_ENCODER_Read(AK_ENCODER_Def* def) {
	GPIO_PinState clk = HAL_GPIO_ReadPin(def->PORT, def->CLK_PIN);
	GPIO_PinState dt = HAL_GPIO_ReadPin(def->PORT, def->DT_PIN);

	//00,01,11,10
	//00,10,11,01

	AK_ENCODER_Value v = 1 * clk + 2 * dt;

	switch (v) {
	case V00:
		_AK_ENCODER_V00(def);
		break;
	case V01:
		_AK_ENCODER_V01(def);
		break;
	case V10:
		_AK_ENCODER_V10(def);
		break;
	case V11:
		_AK_ENCODER_V11(def);
		break;
	}
}

void _AK_ENCODER_Init_Pin(GPIO_TypeDef *port, uint32_t pin, IRQn_Type irq) {
	GPIO_InitTypeDef _def;

	_def.Pin = pin;
	_def.Mode = GPIO_MODE_INPUT;
	_def.Speed = GPIO_SPEED_HIGH;
	_def.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(port, &_def);

//	HAL_NVIC_SetPriority(irq, 0, 0);
//	HAL_NVIC_EnableIRQ(irq);

}

void _AK_ENCODER_V00(AK_ENCODER_Def* def) {
	switch (def->direction) {
	case None:
		break;
	case Left:
	case Rigth:
		if (def->delegator) {
			def->delegator(def->direction);
		}
		def->direction = None;
		break;
	}
}

void _AK_ENCODER_V01(AK_ENCODER_Def* def) {
	switch (def->direction) {
	case None:
		def->direction = Rigth;
		break;
	case Left:
		def->direction = None;
		break;
	}
}

void _AK_ENCODER_V10(AK_ENCODER_Def* def) {
	switch (def->direction) {
	case Rigth:
		def->direction = None;
		break;
	case None:
		def->direction = Left;
		break;
	}
}

void _AK_ENCODER_V11(AK_ENCODER_Def* def) {
}

