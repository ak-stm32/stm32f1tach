/*
 * TimeCounter.c
 *
 *  Created on: 26 May 2016
 *      Author: akoiro
 */

#include "AK_TIME_COUNT.h"

#define ONE_MIN 60000.0
#define ONE_SEC 1000

#define COUNT  5

uint16_t _counters[COUNT];
uint16_t _value = 0;
uint8_t _index = 0;

void AK_TIME_COUNT_Init(void) {
	memset((void*) _counters, 0, (size_t) COUNT * sizeof(_counters[0]));
	_value = 0;
	_index = 0;
}

uint16_t AK_TIME_COUNT_Value(void) {
	uint16_t value = 0;
	for (uint8_t i = 0; i < COUNT; i++) {
		if (i == _index)
			continue;
		value += _counters[i];
	}
	_value = value == 0 ? value : round(ONE_SEC / (value / (COUNT - 1)));
	return _value;
}

void AK_TIME_COUNT_Reset(void) {
	if (_index < (COUNT - 1)) {
		_index++;
	} else {
		_index = 0;
	}
	_counters[_index] = 0;
}

void AK_TIME_COUNT_Increment(void) {
	_counters[_index]++;
}

const struct _AK_TIME_COUNT AK_TIME_COUNT = {
		.Init = AK_TIME_COUNT_Init,
		.Value = AK_TIME_COUNT_Value,
		.Increment = AK_TIME_COUNT_Increment,
		.Reset = AK_TIME_COUNT_Reset };

