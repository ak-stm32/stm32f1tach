/*
 * AK_TIM.c
 *
 *  Created on: 11 ����. 2016 �.
 *      Author: pervoliner
 */

#include "AK_TIM.h"

void AK_TIM_Init(AK_TIM_Def *def) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	__AK_TIM_CLK_ENABLE(def->Instance);

	def->Handle.Instance = def->Instance;
	def->Handle.Init.Prescaler = def->Prescaler;
	def->Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	def->Handle.Init.Period = def->Period;
	def->Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	def->Handle.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&def->Handle);

	HAL_TIM_Base_Start_IT(&def->Handle);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&def->Handle, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&def->Handle, &sMasterConfig);
}

void AK_TIM_MspInit(AK_TIM_Def *def) {
	HAL_NVIC_SetPriority(def->IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(def->IRQn);
}

void AK_TIM_IRQHandler(AK_TIM_Def *def) {
	HAL_TIM_IRQHandler(&def->Handle);
}

