/*
 * Timer2.c
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */
#include "AK_TIM2.h"

TIM_HandleTypeDef _htim2;
/**
 * Timer for FYQ5641
 */
void AK_TIM2_Init(void) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	__TIM2_CLK_ENABLE();
	_htim2.Instance = TIM2;
	_htim2.Init.Prescaler = AK_TIM2_Prescaler;
	_htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	_htim2.Init.Period = AK_TIM2_Period;
	_htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	_htim2.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&_htim2);

	HAL_TIM_Base_Start_IT(&_htim2);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&_htim2, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&_htim2, &sMasterConfig);
}

void AK_TIM2_Msp_Init(void) {
	HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);
}

TIM_HandleTypeDef* AK_TIM2_Get_htim(void) {
	 return &_htim2;
}

//void TIM2_IRQHandler(void) {
//	HAL_TIM_IRQHandler(&_htim2);
//}
//

const struct _AK_TIM AK_TIM2 = {
    .Init = AK_TIM2_Init,
    .Msp_Init = AK_TIM2_Msp_Init,
	.htim = AK_TIM2_Get_htim
};
