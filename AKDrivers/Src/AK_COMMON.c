/*
 * AK_COMMON.c
 *
 *  Created on: 9 ���. 2016 �.
 *      Author: pervoliner
 */

#include "AK_COMMON.h"

void __AK_GPIO_PORT_ENABLE(GPIO_TypeDef *port) {
	if (port == GPIOA) {
		__HAL_RCC_GPIOA_CLK_ENABLE();
	} else if (port == GPIOB) {
		__HAL_RCC_GPIOB_CLK_ENABLE();
	} else if (port == GPIOC) {
		__HAL_RCC_GPIOC_CLK_ENABLE();
	} else if (port == GPIOD) {
		__HAL_RCC_GPIOD_CLK_ENABLE();
	}
}

void __AK_TIM_CLK_ENABLE(TIM_TypeDef *tim) {
	if (tim == TIM1) {
		__HAL_RCC_TIM1_CLK_ENABLE();
	} else if (tim == TIM2) {
		__HAL_RCC_TIM2_CLK_ENABLE();
	} else if (tim == TIM3) {
		__HAL_RCC_TIM3_CLK_ENABLE();
	} else if (tim == TIM4) {
		__HAL_RCC_TIM4_CLK_ENABLE();
	}
}

void __AK_SPI_CLK_ENABLE(SPI_TypeDef * spi) {
	if (spi == SPI1) {
		__HAL_RCC_SPI1_CLK_ENABLE();
	} else if (spi == SPI2) {
		__HAL_RCC_SPI2_CLK_ENABLE();
	}
}

void __AK_SPI_CLK_DISABLE(SPI_TypeDef * spi) {
	if (spi == SPI1) {
		__HAL_RCC_SPI1_CLK_DISABLE();
	} else if (spi == SPI2) {
		__HAL_RCC_SPI2_CLK_DISABLE();
	}
}
