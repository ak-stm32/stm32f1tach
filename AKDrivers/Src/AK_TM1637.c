/*
 * AK_TM1637.c
 *
 *  Created on: 9 ���. 2016 �.
 *      Author: pervoliner
 */

#include "AK_TM1637.h"

void _AK_TM1637_Start(AK_TM1637_Def*);
void _AK_TM1637_Stop(AK_TM1637_Def*);
void _AK_TM1637_CLKLow(AK_TM1637_Def*);
void _AK_TM1637_CLKHigh(AK_TM1637_Def*);
void _AK_TM1637_DIOHigh(AK_TM1637_Def*);
void _AK_TM1637_DIOLow(AK_TM1637_Def*);
void _AK_TM1637_InitWrite(AK_TM1637_Def* def);
void _AK_TM1637_Write(unsigned char, AK_TM1637_Def*);
void _AK_TM1637_Read(AK_TM1637_Def*);
void _AK_TM1637_Delay(unsigned int);

const char _AK_TM1637_segment_Map[] = { 0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d,
		0x7d, 0x07, // 0-7
		0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, // 8-9, A-F
		0x00 };

void AK_TM1637_Init(AK_TM1637_Def* def) {
	__AK_GPIO_PORT_ENABLE(def->PortCLK);
	__AK_GPIO_PORT_ENABLE(def->PortDIO);

	GPIO_InitTypeDef gpioDef;

	gpioDef.Pull = GPIO_PULLUP;
	gpioDef.Mode = GPIO_MODE_OUTPUT_OD; // OD = open drain
	gpioDef.Speed = GPIO_SPEED_FREQ_HIGH;

	gpioDef.Pin = def->PinCLK;
	HAL_GPIO_Init(def->PortCLK, &gpioDef);

	gpioDef.Pin = def->PinDIO;
	HAL_GPIO_Init(def->PortDIO, &gpioDef);
}

void AK_TM1637_Display(int v, int displaySeparator, AK_TM1637_Def* def) {
	unsigned char digitArr[4];
	for (int i = 0; i < 4; i++) {
		digitArr[i] = _AK_TM1637_segment_Map[v % 10];
		if (i == 2 && displaySeparator) {
			//digitArr[i] |= 1 << 7;
		}
		v /= 10;
	}

	_AK_TM1637_InitWrite(def);

	for (int i = 3; i >= 0; i--) {
		_AK_TM1637_Write(digitArr[i], def);
		_AK_TM1637_Read(def);
	}

	_AK_TM1637_Stop(def);
}

void AK_TM1637_Brightness(char brightness, AK_TM1637_Def* def) {
	// Valid brightness values: 0 - 8.
	// 0 = display off.
	// Brightness command:
	// 1000 0XXX = display off
	// 1000 1BBB = display on, brightness 0-7
	// X = don't care
	// B = brightness
	_AK_TM1637_Start(def);
	_AK_TM1637_Write(0x87 + brightness, def);
	_AK_TM1637_Read(def);
	_AK_TM1637_Stop(def);
}

void AK_TM1637_Reset(AK_TM1637_Def* def) {
	_AK_TM1637_InitWrite(def);
	_AK_TM1637_Write(0x00, def);
	_AK_TM1637_Read(def);
	_AK_TM1637_Write(0x00, def);
	_AK_TM1637_Read(def);
	_AK_TM1637_Write(0x00, def);
	_AK_TM1637_Read(def);
	_AK_TM1637_Write(0x00, def);
	_AK_TM1637_Read(def);
	_AK_TM1637_Stop(def);
}

void AK_TM1637_Test(AK_TM1637_Def* def) {
	AK_TM1637_Reset(def);
	for (uint8_t d = 0; d < 4; d++) {
		uint8_t v = 1;
		uint8_t i = 0;
		while (i < 8) {
			uint8_t a = d;
			_AK_TM1637_InitWrite(def);
			while (a > 0) {
				_AK_TM1637_Write(0xFF, def);
				_AK_TM1637_Read(def);
				a--;
			}
			_AK_TM1637_Write(v, def);
			_AK_TM1637_Read(def);
			_AK_TM1637_Stop(def);
			i++;
			v = v | 1 << i;
			_AK_TM1637_Delay(AK_TM1637_TEST_DELAY);
		}
	}
	AK_TM1637_Reset(def);
}

void _AK_TM1637_InitWrite(AK_TM1637_Def* def) {
	_AK_TM1637_Start(def);
	_AK_TM1637_Write(0x40, def);
	_AK_TM1637_Read(def);
	_AK_TM1637_Stop(def);

	_AK_TM1637_Start(def);
	_AK_TM1637_Write(0xc0, def);
	_AK_TM1637_Read(def);
}

void _AK_TM1637_Start(AK_TM1637_Def* def) {
	_AK_TM1637_CLKHigh(def);
	_AK_TM1637_DIOHigh(def);
	_AK_TM1637_Delay(2);
	_AK_TM1637_DIOLow(def);
}

void _AK_TM1637_Stop(AK_TM1637_Def* def) {
	_AK_TM1637_CLKLow(def);
	_AK_TM1637_Delay(2);
	_AK_TM1637_DIOLow(def);
	_AK_TM1637_Delay(2);
	_AK_TM1637_CLKHigh(def);
	_AK_TM1637_Delay(2);
	_AK_TM1637_DIOHigh(def);
}

void _AK_TM1637_Write(unsigned char b, AK_TM1637_Def* def) {
	for (int i = 0; i < 8; ++i) {
		_AK_TM1637_CLKLow(def);
		if (b & 0x01) {
			_AK_TM1637_DIOHigh(def);
		} else {
			_AK_TM1637_DIOLow(def);
		}
		_AK_TM1637_Delay(3);
		b >>= 1;
		_AK_TM1637_CLKHigh(def);
		_AK_TM1637_Delay(3);
	}
}

void _AK_TM1637_Read(AK_TM1637_Def* def) {
	_AK_TM1637_CLKLow(def);
	_AK_TM1637_Delay(5);
	// while (dio); // We're cheating here and not actually reading back the response.
	_AK_TM1637_CLKHigh(def);
	_AK_TM1637_Delay(2);
	_AK_TM1637_CLKLow(def);
}

void _AK_TM1637_Delay(unsigned int i) {
	for (; i > 0; i--) {
		for (int j = 0; j < 10; ++j) {
			__asm__ __volatile__("nop\n\t":::"memory");
		}
	}
}

void _AK_TM1637_CLKHigh(AK_TM1637_Def* def) {
	HAL_GPIO_WritePin(def->PortCLK, def->PinCLK, GPIO_PIN_SET);
}

void _AK_TM1637_CLKLow(AK_TM1637_Def* def) {
	HAL_GPIO_WritePin(def->PortCLK, def->PinCLK, GPIO_PIN_RESET);
}

void _AK_TM1637_DIOHigh(AK_TM1637_Def* def) {
	HAL_GPIO_WritePin(def->PortDIO, def->PinDIO, GPIO_PIN_SET);
}

void _AK_TM1637_DIOLow(AK_TM1637_Def* def) {
	HAL_GPIO_WritePin(def->PortDIO, def->PinDIO, GPIO_PIN_RESET);
}
