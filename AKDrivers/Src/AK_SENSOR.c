/*
 * AK_SENSOR.c
 *
 *  Created on: 23 Jul 2016
 *      Author: akoiro
 */
#include "AK_SENSOR.h"

void AK_SENSOR_Init(void) {
	__GPIOA_CLK_ENABLE();

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = SENSOR_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SENSOR_PORT, &GPIO_InitStructure);

	HAL_NVIC_SetPriority(SENSOR_IRQ, 0, 0);
	HAL_NVIC_EnableIRQ(SENSOR_IRQ);
}

const struct _AK_SENSOR AK_SENSOR = {
    .Init = AK_SENSOR_Init,
};
