/*
 * Timer2.h
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_TIM2_H_
#define AK_TIM2_H_

#include "AK_TIM.h"

//72Mhz/60000 = 1200Hz/3 = 400Hz - 400Hz is the best for us
#ifdef SYS72_TIMER72
#define AK_TIM2_Prescaler 60000
#define AK_TIM2_Period 3
#endif
//18Mhz/45000 = 400Hz - 400Hz is the best for us
#ifdef SYS72_TIMER18
#define AK_TIM2_Prescaler 45000
#define AK_TIM2_Period 1
#endif

//8Mhz/20000 = 400Hz - 400Hz is the best for us
#ifdef SYS8_TIMER8
#define AK_TIM2_Prescaler 20000
#define AK_TIM2_Period 1
#endif



#endif /* AK_TIM2_H_ */
