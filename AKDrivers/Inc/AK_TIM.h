/*
 * AK_TIM.h
 *
 *  Created on: 30 Jul 2016
 *      Author: akoiro
 */

#ifndef AK_TIM_H_
#define AK_TIM_H_
#include "stm32f1xx_hal.h"
#include "AK_COMMON.h"

typedef struct AK_TIM_Def{
	TIM_TypeDef *Instance;
	uint32_t Prescaler;
	uint32_t Period;
	IRQn_Type IRQn;

	TIM_HandleTypeDef Handle;
} AK_TIM_Def;

void AK_TIM_Init(AK_TIM_Def*);
void AK_TIM_IRQHandler(AK_TIM_Def*);
void AK_TIM_MspInit(AK_TIM_Def*);

const struct _AK_TIM {
	TIM_HandleTypeDef* (*htim) (void);
    void (*Init)(void);
    void (*Msp_Init)(void);
} _AK_TIM;

#endif /* AK_TIM_H_ */
