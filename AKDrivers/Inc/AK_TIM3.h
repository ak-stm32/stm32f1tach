/*
 * Timer3.h
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_TIM3_H_
#define AK_TIM3_H_

#include "AK_TIM.h"

//72Mhz/1000 = 72000Hz/72 = 1000Hz
#ifdef SYS72_TIMER72
#define AK_TIM3_Prescaler 1000
#define AK_TIM3_Period 72
#endif
//8Mhz/100 = 80000Hz/80 = 1000Hz
#ifdef SYS8_TIMER8
#define AK_TIM3_Prescaler 100
#define AK_TIM3_Period 80
#endif
//18Mhz/1000 = 18000Hz/18 = 1000Hz
#ifdef SYS72_TIMER18
#define AK_TIM3_Prescaler 1000
#define AK_TIM3_Period 18
#endif


#endif /* AK_TIM3_H_ */
