/*
 * AK_TM1637.h
 *
 *  Created on: 9 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_TM1637_H_
#define AK_TM1637_H_

#include "AK_COMMON.h"

#define AK_TM1637_TEST_DELAY  30000

typedef struct
{
	//TM1637:18
	GPIO_TypeDef *PortCLK;
	uint32_t PinCLK;
	//TM1637:17
	GPIO_TypeDef* PortDIO;
	uint32_t PinDIO;
} AK_TM1637_Def;


void AK_TM1637_Init(AK_TM1637_Def*);
void AK_TM1637_Brightness(char,AK_TM1637_Def*);
void AK_TM1637_Display(int, int, AK_TM1637_Def*);
void AK_TM1637_Test(AK_TM1637_Def* def);




#endif /* AK_TM1637_H_ */
