/*
 * AK_SENSOR.h
 *
 *  Created on: 23 Jul 2016
 *      Author: akoiro
 */

#ifndef AK_SENSOR_H_
#define AK_SENSOR_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"

#define SENSOR_PIN		GPIO_PIN_1
#define SENSOR_PORT		GPIOA
#define SENSOR_IRQ 		EXTI1_IRQn

const struct _AK_SENSOR {
    void (*Init)(void);
} _AK_SENSOR;


#endif /* AK_SENSOR_H_ */
