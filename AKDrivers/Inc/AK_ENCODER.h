/*
 * AK_ENCODER.h
 *
 *  Created on: 1 Aug 2016
 *      Author: akoiro
 */

#ifndef AK_ENCODER_H_
#define AK_ENCODER_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "AK_COMMON.h"

typedef enum {
	 Left = 1,
	 Rigth = 2,
	 None = 0
} AK_ENCODER_Direction;


typedef enum {
	 V00 = 0,
	 V01 = 1,
	 V10 = 2,
	 V11 = 3
} AK_ENCODER_Value;

typedef void (*AK_ENCODER_Delegator)(AK_ENCODER_Direction);

typedef struct {
	uint16_t CLK_PIN;
	IRQn_Type CLK_IRQ;
	uint16_t DT_PIN;
	IRQn_Type DT_IRQ;
	uint16_t SW_PIN;
	IRQn_Type SW_IRQ;
	GPIO_TypeDef *PORT;
	AK_ENCODER_Direction direction;
	AK_ENCODER_Delegator delegator;
} AK_ENCODER_Def;


void AK_ENCODER_Init(AK_ENCODER_Def* def);
void AK_ENCODER_Read(AK_ENCODER_Def* def);

#endif /* AK_ENCODER_H_ */
