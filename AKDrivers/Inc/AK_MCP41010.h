/*
 * AK_MCP41010.h
 *
 *  Created on: 16 Oct 2016
 *      Author: akoiro
 */

#ifndef AK_MCP41010_H_
#define AK_MCP41010_H_

#include "AK_COMMON.h"

#define _AK_MCP41010_CMD ((uint8_t)  0b00010001)

#define AK_MCP41010_SPI2

#ifdef AK_MCP41010_SPI1
/**SPI1 GPIO Configuration
PA4     ------> SPI1_NSS
PA5     ------> SPI1_SCK
PA7     ------> SPI1_MOSI
*/
#define NSS_PIN GPIO_PIN_4
#define SCK_PIN GPIO_PIN_5
#define MOSI_PIN GPIO_PIN_7
#define SPI_PORT GPIOA
#endif

#ifdef AK_MCP41010_SPI2
/**SPI2 GPIO Configuration
PA12     ------> SPI2_NSS
PB13     ------> SPI2_SCK
PB15     ------> SPI2_MOSI
*/
#define NSS_PIN GPIO_PIN_12
#define SCK_PIN GPIO_PIN_13
#define MOSI_PIN GPIO_PIN_15
#define SPI_PORT GPIOB
#endif


typedef struct AK_MCP41010_HandleDef{
	SPI_TypeDef *SPIType;

	uint16_t NSS_Pin;
	uint16_t SCK_Pin;
	uint16_t MOSI_Pin;
	GPIO_TypeDef *GPIOType;

	SPI_HandleTypeDef SPIHandle;
	uint8_t Value[2];

} AK_MCP41010_HandleDef;

void AK_MCP41010_Init(AK_MCP41010_HandleDef* handle);
void AK_MCP41010_MspInit(AK_MCP41010_HandleDef* handle);
void AK_MCP41010_MspDeInit(AK_MCP41010_HandleDef* handle);
void AK_MCP41010_Write(uint8_t value, AK_MCP41010_HandleDef* handle);
#endif /* AK_MCP41010_H_ */
