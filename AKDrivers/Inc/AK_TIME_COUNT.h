/*
 * AK_TIME_COUNT.h
 *
 *  Created on: 23 Jul 2016
 *      Author: akoiro
 */

#ifndef AK_TIME_COUNT_H_
#define AK_TIME_COUNT_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const struct _AK_TIME_COUNT {
	void (*Init)(void);
	uint16_t (*Value) (void);
	void (*Increment)(void);
	void (*Reset)(void);
} _AK_TIME_COUNT;

#endif /* AK_TIME_COUNT_H_ */
