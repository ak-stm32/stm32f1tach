/*
 * main_test.c
 *
 *  Created on: 9 ���. 2016 �.
 *      Author: pervoliner
 */

#ifdef TEST

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "stm32f1xx_hal.h"

#include "../AKDrivers/Inc/AK_TM1637.h"
#include "../AKDrivers/Inc/AK_MCP41010.h"
#include "../AKDrivers/Inc/AK_ENCODER.h"
#include "../AKDrivers/Inc/AK_TIM.h"

void _Handle_Encoder1_Value(AK_ENCODER_Direction value);

AK_TM1637_Def _display1;
AK_TM1637_Def _display2;
AK_TM1637_Def _display3;

AK_ENCODER_Def _encoder1;

AK_TIM_Def _MAIN_TIM;
AK_MCP41010_HandleDef _MCP41010_1;

char _Brightness = 4;
uint32_t _OUTPUT = 128;


//18Mhz/45000 = 400Hz - 400Hz is the best for us

GPIO_PinState _pin4;


void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler */
}

void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV8;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
		Error_Handler();
	}

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

void _Init_Display1(void) {
	_display1.PortDIO = GPIOC;
	_display1.PortCLK = GPIOC;
	_display1.PinDIO = GPIO_PIN_13;
	_display1.PinCLK = GPIO_PIN_14;

	AK_TM1637_Init(&_display1);
}

void _Init_Display2(void) {
	_display2.PortDIO = GPIOC;
	_display2.PortCLK = GPIOA;
	_display2.PinDIO = GPIO_PIN_15;
	_display2.PinCLK = GPIO_PIN_0;

	AK_TM1637_Init(&_display2);
}

void _Init_Display3(void) {
	_display3.PortDIO = GPIOA;
	_display3.PortCLK = GPIOA;
	_display3.PinDIO = GPIO_PIN_1;
	_display3.PinCLK = GPIO_PIN_2;

	AK_TM1637_Init(&_display3);
}


void _Init_Encoder1(void) {
	_encoder1.PORT = GPIOB;
//	_encoder1.CLK_IRQ = EXTI15_10_IRQn;
//	_encoder1.DT_IRQ = EXTI15_10_IRQn;
//	_encoder1.SW_IRQ = EXTI15_10_IRQn;
	_encoder1.CLK_PIN = GPIO_PIN_11;
	_encoder1.DT_PIN = GPIO_PIN_10;
	_encoder1.SW_PIN = GPIO_PIN_1;
	_encoder1.delegator = _Handle_Encoder1_Value;

	AK_ENCODER_Init(&_encoder1);
}

void _Init_MCP41010_1(void) {
	_MCP41010_1.GPIOType = GPIOB;
	_MCP41010_1.NSS_Pin = GPIO_PIN_12;
	_MCP41010_1.SCK_Pin = GPIO_PIN_13;
	_MCP41010_1.MOSI_Pin = GPIO_PIN_15;
	_MCP41010_1.SPIType = SPI2;

	AK_MCP41010_Init(&_MCP41010_1);
}

void _Init_MAIN_TIM(void) {

	_MAIN_TIM.Instance = TIM2;
	_MAIN_TIM.Prescaler = 45000;
	_MAIN_TIM.Period = 1;
	_MAIN_TIM.Instance = TIM2;
	_MAIN_TIM.IRQn = TIM2_IRQn;

	AK_TIM_Init(&_MAIN_TIM);
}

void _Handle_Encoder1_Value(AK_ENCODER_Direction value) {
	switch(value) {
		case Left:
			//_Brightness -= _Brightness > 0 ? 1:0;
			_OUTPUT -= _OUTPUT > 0 ? 1:0;
			break;
		case Rigth:
			//_Brightness += _Brightness < 7 ? 1:0;
			_OUTPUT += _OUTPUT < 255 ? 1:0;
	}
	//AK_TM1637_Brightness(_Brightness, &_display1);
	AK_TM1637_Display(_OUTPUT, 1, &_display3);
	AK_MCP41010_Write(_OUTPUT, &_MCP41010_1);
}

int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	_Init_MCP41010_1();

	_Init_Encoder1();

	_Init_Display1();
	_Init_Display2();
	_Init_Display3();

	_Init_MAIN_TIM();

	AK_TM1637_Brightness(_Brightness, &_display1);
	AK_TM1637_Test(&_display1);
	AK_TM1637_Test(&_display2);
	AK_TM1637_Test(&_display3);
	AK_MCP41010_Write(_OUTPUT,  &_MCP41010_1);
	AK_TM1637_Display(_OUTPUT, 1,  &_display3);

//   	uint8_t i = 0;
	while (1) {
//		AK_TM1637_Display(1234, 1, &_display1);
//		_AK_TM1637_Delay(100);
//		if (i < 256) {
			//AK_MCP41010_Write(i, &_MCP41010_1);
//		   	_AK_TM1637_Delay(1);
//		    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
//		   	_AK_TM1637_Delay(1);
//		    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
//		   	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
//		   	_value[1] = i;
////			_value32[1] = i;
//			HAL_SPI_Transmit(&hspi1, _value, 2,  1000);
//		   	_AK_TM1637_Delay(1000);
//		   	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);

//		   	i++;
//		} else {
//			i = 0;
//		}

	}
	/* USER CODE END 3 */

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == _MAIN_TIM.Instance) {
       AK_ENCODER_Read(&_encoder1);
//		AK_TM1637_Brightness(_Brightness, &_display1);
	}
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == _encoder1.CLK_PIN ||
			GPIO_Pin == _encoder1.DT_PIN ||
			GPIO_Pin == _encoder1.SW_PIN) {
		AK_ENCODER_Read(&_encoder1);
	}
}

#endif

